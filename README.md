# bloomex

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

---
### Configuration

Create ```src/contantsBag/ServerLocal.js``` file

Add constant for Backend HOST

Example:
```
export const API_HOST = 'http://bloomex-api.local/api';
```
