export default [
    {
        to: 'mainPage',
        label: 'Home'
    },
    {
        to: 'usersList',
        label: 'Users'
    },
    {
        to: 'customersList',
        label: 'Customers'
    }
];
