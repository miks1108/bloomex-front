import MainView from "@/views/MainView.vue";
import UsersView from "@/views/UsersView.vue";
import CustomersView from "@/views/CustomersView.vue";

export default [
    {
        path: '/',
        name: 'mainPage',
        component: MainView
    },
    {
        path: '/users',
        name: 'usersList',
        component: UsersView
    },
    {
        path: '/customers',
        name: 'customersList',
        component: CustomersView
    }
];
