export class LinkDto {
    /**
     * @type {string}
     */
    url = '';

    /**
     * @type {string}
     */
    label = '';

    /**
     * @type {boolean}
     */
    active = false;
}
