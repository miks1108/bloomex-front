export class TableDataDto {
    /**
     * @param {String[]} attributes
     * @param {Object} labels
     * @param {Object[]} data
     * @param {Object} sortable
     */
    constructor(attributes, labels, data, sortable = []) {
        this.attributes = attributes;
        this.labels = labels;
        this.data = data;
        this.sortable = sortable
    }

    /**
     * @type {Object}
     */
    filters = {};
}
