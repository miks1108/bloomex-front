import TableWidget from "@/components/UI/TableWidget.vue";
import PagerWidget from "@/components/UI/PagerWidget.vue";

export default [
    TableWidget,
    PagerWidget
];
