export class UserEntity {

    /**
     * @type {number|null}
     */
    id = null;

    /**
     * @type {string}
     */
    name = '';

    /**
     * @type {string}
     */
    email = '';

    /**
     * @type {string}
     */
    created_at = '';
}
