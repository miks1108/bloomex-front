export class PhoneNumberEntity {
    /**
     * @type {string}
     */
    number = '';

    /**
     * @type {string}
     */
    type = '';
}
