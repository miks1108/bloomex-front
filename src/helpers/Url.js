/**
 * @param {string} param
 * @param {any} value
 * @returns void
 */
export function changeUrl(param, value) {
    const url = new URL(window.location.href);
    url.searchParams.set(param, value);
    window.history.pushState({}, "", url);
}

/**
 * @param {string|null} url
 * @returns int
 */
export function getPageFromUrl(url = null) {
    if (url === null) {
        url = window.location.href;
    }

    return parseInt(new URL(url).searchParams.get('page'));
}

/**
 * @param {string|null} url
 * @returns string|null
 */
export function getSortFromUrl(url = null) {
    if (url === null) {
        url = window.location.href;
    }

    return new URL(url).searchParams.get('sort');
}

/**
 * @param {string} param
 * @returns void
 */
export function removeParamFromUrl(param) {
    const url = new URL(window.location.href);
    url.searchParams.delete(param);
    window.history.pushState({}, "", url);
}
