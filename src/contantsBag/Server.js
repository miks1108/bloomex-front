import {API_HOST} from "@/contantsBag/ServerLocal";

export const USERS_LIST_URL = API_HOST + '/users';

export const CUSTOMERS_LIST_URL = API_HOST + '/customers';
