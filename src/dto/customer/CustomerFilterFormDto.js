export class CustomerFilterFormDto {
    /**
     * @type {int|null}
     */
    id = null;

    /**
     * @type {string}
     */
    first_name = '';

    /**
     * @type {string}
     */
    last_name = '';

    /**
     * @type {string}
     */
    email_address = '';

    /**
     * @type {string}
     */
    full_street_address = '';

    /**
     * @type {string}
     */
    phone_number = '';

    /**
     * @type {string}
     */
    created_at = '';
}
