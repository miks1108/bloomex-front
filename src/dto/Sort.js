export class Sort {
    /**
     * @type {string}
     */
    attribute = '';

    /**
     * @type {boolean}
     */
    asc = true;

    /**
     * @returns {string}
     */
    getSort() {
        if (!this.attribute) {
            return '';
        }
        let sortParam = '';
        if (!this.asc) {
            sortParam = '-';
        }

        return sortParam + this.attribute;
    }
}
